package com.app.voltmusic.data;

import android.arch.persistence.room.Room;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;
import com.app.voltmusic.App;
import com.app.voltmusic.data.model.LoggedInUser;
import com.app.voltmusic.database.VoltMusiqueDatabase;
import com.app.voltmusic.database.entite.Utilisateur;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {
    private Utilisateur loggedUser = new Utilisateur();
    private VoltMusiqueDatabase db = Room.databaseBuilder(App.getContext(),
            VoltMusiqueDatabase.class, "voltmusique").build();

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication (appel API)
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(App.getContext());

            String url = "https://volt-music.herokuapp.com/login";
            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("email", "ubald@gmail.com");
            postParam.put("password", "azerty");

            JSONObject params = new JSONObject(postParam);
            RequestFuture<JSONObject> future = RequestFuture.newFuture();
            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, url,
                params,
                future,
                future) {
                @Override
                public Map<String, String> getHeaders()  {
                    HashMap<String, String> headers = new HashMap<>();
                    // headers.put("Accept", "application/json");
                    headers.put("Content-Type", "application/json");
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                        System.out.println(response.headers);
                        JSONObject result = new JSONObject();
                        result.put("header", new JSONObject(response.headers));
                        result.put("body", new JSONObject(jsonString));

                        return Response.success(result,
                                HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            };

            queue.add(req);

            boolean isLogged = false;

            try {
                System.out.println("En attente....");
                JSONObject response = future.get(60, TimeUnit.SECONDS);
                HashMap<String, String> resHeader;
                HashMap<String, String> resBody;

                try {
                    resHeader = new Gson().fromJson(response.get("header").toString(), HashMap.class);
                    resBody = new Gson().fromJson(response.get("body").toString(), HashMap.class);
                } catch(JSONException e) {
                    resHeader = null;
                    resBody = null;
                    System.out.println("Erreur : " + e.getMessage());
                }

                if(!resHeader.isEmpty() && !resBody.isEmpty() && !resHeader.get("Authorization").isEmpty()) {
                    loggedUser.pseudo = resBody.get("pseudo");
                    loggedUser.nom = resBody.get("nom");
                    loggedUser.prenom = resBody.get("prenom");
                    loggedUser.numEtu = Integer.parseInt(resBody.get("num_etu"));
                    loggedUser.role = resBody.get("role");
                    loggedUser.dateNaissance = resBody.get("date_naissance");
                    loggedUser.userToken = resHeader.get("Authorization");
                    db.utilisateurDao().insertUser(loggedUser);
                    System.out.println("SUCCES! " + resHeader.get("Authorization"));
                    System.out.println("Réussite de la connexion.");
                    isLogged = true;
                } else {
                    System.out.println("Echec de la connexion.");
                }
            } catch (InterruptedException e) {
                System.out.println("Erreur: "+e.toString());
                throw new Exception("Echec de la récupération de la connexion.");
            }

            if(isLogged) {
                LoggedInUser user =
                    new LoggedInUser(
                            loggedUser.userToken,
                            loggedUser.pseudo,
                            loggedUser.prenom,
                            loggedUser.nom,
                            loggedUser.email,
                            loggedUser.dateNaissance,
                            loggedUser.numEtu,
                            loggedUser.role);
                return new Result.Success<>(user);
            } else {
                throw new Exception("Une erreur est survenue durant la connexion.");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new Result.Error(new IOException("Erreur lors de la tentative de connexion : ", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication (appel API)
        db.utilisateurDao().deleteAllUsers();
    }
}