package com.app.voltmusic.data.model;

public class MusicFile {
    private String path;
    private String titre;
    private String artist;
    private String album;
    private String duration;
    private String date_sortie;

    public MusicFile(String path, String titre, String artist, String album, String duration, String date_sortie) {
        this.path = path;
        this.titre = titre;
        this.artist = artist;
        this.album = album;
        this.duration = duration;
        this.date_sortie=date_sortie;
    }

    public MusicFile() {
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDate_sortie() {
        return date_sortie;
    }

    public void setDate_sortie(String date_sortie) {
        this.date_sortie = date_sortie;
    }
}
