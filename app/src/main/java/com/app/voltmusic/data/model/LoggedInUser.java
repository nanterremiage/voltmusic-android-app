package com.app.voltmusic.data.model;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;

import com.app.voltmusic.App;
import com.app.voltmusic.database.VoltMusiqueDatabase;
import com.app.voltmusic.database.entite.Utilisateur;

/**
 * Data class that captures user information for logged in users retrieved from LoginRepository
 */
public class LoggedInUser {

    private String userToken = null;
    private String pseudo = null;
    private String prenom = null;
    private String nom = null;
    private String email = null;
    private String dateNaissance = null;
    private Integer numEtu = null;
    private String role = null;

    public LoggedInUser() {
        VoltMusiqueDatabase db = Room.databaseBuilder(App.getContext(),
                VoltMusiqueDatabase.class, "voltmusique").allowMainThreadQueries().build();
        Utilisateur user = db.utilisateurDao().getUser();

        if(user != null) {
            this.userToken = user.userToken;
            this.pseudo = user.pseudo;
            this.prenom = user.prenom;
            this.nom = user.nom;
            this.email = user.email;
            this.dateNaissance = user.dateNaissance;
            this.numEtu = user.numEtu;
            this.role = user.role;
        }
    }

    public LoggedInUser(String userToken, String pseudo, String prenom, String nom, String email,
            String dateNaissance, Integer numEtu, String role) {
        this.userToken = userToken;
        this.pseudo = pseudo;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.dateNaissance = dateNaissance;
        this.numEtu = numEtu;
        this.role = role;
    }

    public String getUserToken() {
        return userToken;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public Integer getEtu() {
        return numEtu;
    }

    public String getRole() {
        return role;
    }

}