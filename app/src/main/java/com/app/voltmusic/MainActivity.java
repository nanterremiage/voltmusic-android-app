package com.app.voltmusic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.app.voltmusic.data.model.LoggedInUser;
import com.app.voltmusic.database.entite.Utilisateur;
import com.app.voltmusic.ui.login.LoginActivity;
import com.app.voltmusic.ui.register.RegisterActivity;
import com.app.voltmusic.ui.welcome.WelcomeActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LoggedInUser user = new LoggedInUser();
        if(user.getUserToken() != null) {
            Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(intent);
            String welcome = getString(R.string.welcome) + user.getPseudo();
            Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
        }
        setContentView(R.layout.activity_main);
    }

    public void logIn(View view) {
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void signIn(View view) {
        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}
