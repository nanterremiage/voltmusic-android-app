package com.app.voltmusic.ui.welcome.ui.music_ablum_fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.voltmusic.R;
import com.app.voltmusic.ui.welcome.ui.adapter.MusicAdapter;
import com.app.voltmusic.ui.welcome.ui.personal.FavoritesFragment;


public class MusicFragment extends Fragment {
     RecyclerView recyclerView;
     MusicAdapter musicAdapter;

    public MusicFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_music, container, false);
        recyclerView=view.findViewById(R.id.recyleView);
        recyclerView.setHasFixedSize(true);
        musicAdapter=new MusicAdapter(getContext(), FavoritesFragment.audio_list);
        recyclerView.setAdapter(musicAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),recyclerView.VERTICAL,false));
        return view;
    }
}