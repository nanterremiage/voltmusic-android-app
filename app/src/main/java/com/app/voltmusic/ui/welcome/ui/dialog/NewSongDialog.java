package com.app.voltmusic.ui.welcome.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.voltmusic.R;

public class NewSongDialog extends AppCompatDialogFragment {
    private EditText album_name;
    Button uploadFileBtn;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.layout_dialog_new_song, null);

        builder.setView(v).setTitle("Nouveau song")
                .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setPositiveButton("Valider", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        album_name = v.findViewById(R.id.new_album_btn);
        uploadFileBtn = (Button)v.findViewById(R.id.upload_file_btn);
        return builder.create();
    }
}