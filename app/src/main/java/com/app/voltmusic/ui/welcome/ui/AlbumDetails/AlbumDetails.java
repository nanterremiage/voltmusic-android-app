package com.app.voltmusic.ui.welcome.ui.AlbumDetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.app.voltmusic.R;
import com.app.voltmusic.data.model.MusicFile;
import com.app.voltmusic.ui.welcome.ui.adapter.AlbumDetailsAdapter;

import java.util.ArrayList;

import static com.app.voltmusic.ui.welcome.ui.personal.FavoritesFragment.audio_list;

public class AlbumDetails extends AppCompatActivity {
    RecyclerView recyclerView;
    ImageView albumPhoto;
    String albumName;
    AlbumDetailsAdapter albumDetailsAdapter;
    ArrayList<MusicFile> albumSongs = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_details);
        recyclerView = findViewById(R.id.recyleView_album);
        albumPhoto = findViewById(R.id.album_photo);
        albumName = getIntent().getStringExtra("albumName");
        int j = 0;
        for (int i =0 ;i<audio_list.size() ; i++){
            if(albumName.equals(audio_list.get(i).getAlbum())){
                albumSongs.add(j, audio_list.get(i));
                j++;
            }
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (albumSongs.size()<1){
            albumDetailsAdapter = new AlbumDetailsAdapter(this, albumSongs);
            recyclerView.setAdapter(albumDetailsAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        }
    }
}