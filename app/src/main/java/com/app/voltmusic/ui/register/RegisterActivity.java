package com.app.voltmusic.ui.register;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.voltmusic.App;
import com.app.voltmusic.R;
import com.app.voltmusic.database.entite.Utilisateur;
import com.app.voltmusic.database.repository.UtilisateurRepository;
import com.app.voltmusic.ui.login.LoginActivity;
import com.app.voltmusic.ui.welcome.WelcomeActivity;
import com.app.voltmusic.utils.*;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    private UtilisateurRepository utilisateurRepo = new UtilisateurRepository(App.getApplication());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        new DateInputMask(((EditText) this.findViewById(R.id.registerDateOfBirthday)));
    }

    public void createAccount(View view) {
        System.out.println("Validation du formulaire...");
        String pseudo = ((EditText) this.findViewById(R.id.registerPseudo)).getText().toString();
        String nom = ((EditText) this.findViewById(R.id.registerLastName)).getText().toString();
        String prenom = ((EditText) this.findViewById(R.id.registerFirstName)).getText().toString();
        String dateNaissance = ((EditText) this.findViewById(R.id.registerDateOfBirthday)).getText().toString();
        String numEtudiant = ((EditText) this.findViewById(R.id.registerNumEtu)).getText().toString();
        String email = ((EditText) this.findViewById(R.id.registerEmail)).getText().toString();
        String password = ((EditText) this.findViewById(R.id.registerPassword)).getText().toString();
        if (pseudo.isEmpty() || nom.isEmpty() || prenom.isEmpty() || dateNaissance.isEmpty() ||
                !Validation.isValidNumber(numEtudiant) || !Validation.isValidEmail(email) || !Validation.isValidPassword(password)) {
            ((EditText) this.findViewById(R.id.registerPseudo)).setError("Champ obligatoire.");
            ((EditText) this.findViewById(R.id.registerLastName)).setError("Champ obligatoire.");
            ((EditText) this.findViewById(R.id.registerFirstName)).setError("Champ obligatoire.");
            ((EditText) this.findViewById(R.id.registerDateOfBirthday)).setError("Champ obligatoire.");
            ((EditText) this.findViewById(R.id.registerNumEtu)).setError("Champ obligatoire.");
            ((EditText) this.findViewById(R.id.registerEmail)).setError("Champ obligatoire.");
            ((EditText) this.findViewById(R.id.registerPassword)).setError("Champ obligatoire.");
        } else {
            System.out.println("Création du compte...");
            String[] dateTab = dateNaissance.split("/");
            RequestQueue queue = Volley.newRequestQueue(App.getContext());
            Map<String, String> postParam = new HashMap<String, String>();
            postParam.put("pseudo", pseudo);
            postParam.put("prenom", prenom);
            postParam.put("nom", nom);
            postParam.put("dateNaissance", dateTab[2]+"-"+dateTab[1]+"-"+dateTab[0]);
            postParam.put("role", "USER");
            postParam.put("numEtu", numEtudiant);
            postParam.put("email", email);
            postParam.put("password", password);

            JSONObject params = new JSONObject(postParam);

            System.out.println(params.toString());

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    "https://volt-music.herokuapp.com/utilisateur/signup",
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Votre compte a été créé avec succès.", Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("Erreur: " + error.toString());
                            Toast.makeText(getApplicationContext(), "La tentative d'inscription a échouée.", Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders()  {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString;

                        System.out.println(response.headers);
                        JSONObject result = new JSONObject();
                        System.out.println("reception...");
                        result.put("header", new JSONObject(response.headers));
                        if(response.data.length > 0) {
                            jsonString = new String(response.data,
                                    HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                            result.put("body", new JSONObject(jsonString));
                        } else {
                            result.put("body", new JSONObject("{\"sucess\":\"true\"}"));
                        }

                        return Response.success(result,
                                HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException je) {
                        return Response.error(new ParseError(je));
                    }
                }
            };

            queue.add(req);
        }
    }
}