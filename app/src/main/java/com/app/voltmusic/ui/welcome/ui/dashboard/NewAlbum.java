package com.app.voltmusic.ui.welcome.ui.dashboard;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.app.voltmusic.R;
import com.app.voltmusic.ui.welcome.ui.dialog.NewSongDialog;

public class NewAlbum extends AppCompatActivity {

    private Button newSongBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_album);

        newSongBtn = (Button) findViewById(R.id.new_song_btn);
        newSongBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNewSongDialog();
            }
        });

    }

    public void openNewSongDialog(){
        NewSongDialog newSongDialog = new NewSongDialog();
        newSongDialog.show(this.getSupportFragmentManager(),
                "Nouveau song");
    }
}