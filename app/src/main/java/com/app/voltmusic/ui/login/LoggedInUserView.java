package com.app.voltmusic.ui.login;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Room;

import com.app.voltmusic.App;
import com.app.voltmusic.database.VoltMusiqueDatabase;
import com.app.voltmusic.database.entite.Utilisateur;
import com.app.voltmusic.database.repository.UtilisateurRepository;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private String userToken;
    private String pseudo;
    private String prenom;
    private String nom;
    private String email;
    private String dateNaissance;
    private Integer numEtu;
    private String role;

    public LoggedInUserView() {
        VoltMusiqueDatabase db = Room.databaseBuilder(App.getContext(),
                VoltMusiqueDatabase.class, "voltmusique").allowMainThreadQueries().build();
        Utilisateur user = db.utilisateurDao().getUser();

        System.out.println(user.pseudo);
        if(!user.userToken.isEmpty()) {
            this.userToken = user.userToken;
            this.pseudo = user.pseudo;
            this.prenom = user.prenom;
            this.nom = user.nom;
            this.email = user.email;
            this.dateNaissance = user.dateNaissance;
            this.numEtu = user.numEtu;
            this.role = user.role;
        }
    }

    public LoggedInUserView(String userToken, String pseudo, String prenom, String nom, String email,
            String dateNaissance, Integer numEtu, String role) {
        this.userToken = userToken;
        this.pseudo = pseudo;
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.dateNaissance = dateNaissance;
        this.numEtu = numEtu;
        this.role = role;
    }

    public String getUserToken() {
        return userToken;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public Integer getEtu() {
        return numEtu;
    }

    public String getRole() {
        return role;
    }
}