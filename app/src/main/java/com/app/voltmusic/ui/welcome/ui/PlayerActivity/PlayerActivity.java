package com.app.voltmusic.ui.welcome.ui.PlayerActivity;

import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.app.voltmusic.R;
import com.app.voltmusic.data.model.MusicFile;

import java.util.ArrayList;

import static com.app.voltmusic.ui.welcome.ui.personal.FavoritesFragment.audio_list;

public class PlayerActivity extends AppCompatActivity {
    ImageView img_player, prev, suivant;
    TextView song_name, duration_played, duration_total;
    SeekBar seekbar_value;
    FloatingActionButton play_pause;
    int position = -1;
    static ArrayList<MusicFile> songs = new ArrayList<>();
    static Uri uri;
    static MediaPlayer mediaPlayer;
    private Handler handler = new Handler();
    private Thread playThread, previousThread, nextThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        InitialiseViews();
        getIntentMethod();
        int total_duration = Integer.parseInt(songs.get(position).getDuration()) / 1000;
        duration_total.setText(Timeformat(total_duration));
        song_name.setText(songs.get(position).getTitre() + songs.get(position).getArtist());
        seekbar_value.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mediaPlayer != null && b) {
                    mediaPlayer.seekTo(i * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        PlayerActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mediaPlayer != null) {
                    int current_position = mediaPlayer.getCurrentPosition() / 1000;
                    seekbar_value.setProgress(current_position);
                    duration_played.setText(Timeformat(current_position));
                }
                handler.postDelayed(this, 1000);
            }
        });
    }

    private String Timeformat(int current_position) {
        String seconds = String.valueOf(current_position % 60);
        String minutes = String.valueOf(current_position / 60);
        if (seconds.length() == 1) {
            return minutes + ":" + "0" + seconds;
        } else return minutes + ":" + seconds;
    }

    private void getIntentMethod() {
        position = getIntent().getIntExtra("position", -1);
        songs = audio_list;
        if (songs != null) {
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            uri = Uri.parse(songs.get(position).getPath());
        }
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            mediaPlayer.start();
        } else {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            mediaPlayer.start();
        }
        seekbar_value.setMax(mediaPlayer.getDuration() / 1000);
    }

    //A function to initialise the views
    public void InitialiseViews() {
        img_player = findViewById(R.id.img_player);
        prev = findViewById(R.id.prev);
        suivant = findViewById(R.id.suivant);
        song_name = findViewById(R.id.song_name);
        duration_played = findViewById(R.id.duration_played);
        duration_total = findViewById(R.id.duration_total);
        seekbar_value = findViewById(R.id.seekbar_value);
        play_pause = findViewById(R.id.play_pause);


    }

    @Override
    protected void onPostResume() {
        playThreadBtn();
        nextThreadBtn();
        previousThreadBtn();
        super.onPostResume();
    }

    private void playThreadBtn() {
        playThread = new Thread(){
            @Override
            public void run() {
                super.run();
                play_pause.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        playPausedClicked();
                    }
                });
            }
        };
        playThread.start();
    }

    private void playPausedClicked() {
        if(mediaPlayer.isPlaying()){
            play_pause.setImageResource(R.drawable.ic__play_circle);
            mediaPlayer.pause();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
        }else{
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            mediaPlayer.start();
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
        }
    }

    private void previousThreadBtn() {
        previousThread = new Thread(){
            @Override
            public void run() {
                super.run();
                prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        prevClicked();
                    }
                });
            }
        };
        previousThread.start();
    }

    private void prevClicked() {
        if (mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            position = ((position - 1)<0? songs.size()-1 : position - 1);
            uri = Uri.parse(songs.get(position).getPath());
            mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            song_name.setText(songs.get(position).getTitre() + songs.get(position).getArtist());
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            mediaPlayer.start();
        }else{
            mediaPlayer.stop();
            mediaPlayer.release();
            position = ((position - 1)<0? songs.size()-1 : position - 1);
            uri = Uri.parse(songs.get(position).getPath());
            mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            song_name.setText(songs.get(position).getTitre() + songs.get(position).getArtist());
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic__play_circle);
        }
    }

    private void nextThreadBtn() {
        nextThread = new Thread(){
            @Override
            public void run() {
                super.run();
                suivant.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        nextClicked();
                    }
                });
            }
        };
        nextThread.start();
    }

    private void nextClicked() {
        if (mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer.release();
            position = ((position + 1) % songs.size());
            uri = Uri.parse(songs.get(position).getPath());
            mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            song_name.setText(songs.get(position).getTitre() + songs.get(position).getArtist());
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic_pause_circle);
            mediaPlayer.start();
        }else{
            mediaPlayer.stop();
            mediaPlayer.release();
            position = ((position + 1) % songs.size());
            uri = Uri.parse(songs.get(position).getPath());
            mediaPlayer = MediaPlayer.create(getApplicationContext(), uri);
            song_name.setText(songs.get(position).getTitre() + songs.get(position).getArtist());
            seekbar_value.setMax(mediaPlayer.getDuration()/1000);
            PlayerActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mediaPlayer != null) {
                        int current_position = mediaPlayer.getCurrentPosition() / 1000;
                        seekbar_value.setProgress(current_position);
                    }
                    handler.postDelayed(this, 1000);
                }
            });
            play_pause.setImageResource(R.drawable.ic__play_circle);
        }
    }
}