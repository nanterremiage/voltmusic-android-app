package com.app.voltmusic.ui.welcome.ui.music_ablum_fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.app.voltmusic.R;
import com.app.voltmusic.ui.welcome.ui.adapter.AlbumAdapter;
import com.app.voltmusic.ui.welcome.ui.adapter.MusicAdapter;
import com.app.voltmusic.ui.welcome.ui.personal.FavoritesFragment;


public class AlbumFragment extends Fragment {
    RecyclerView recyclerView;
    AlbumAdapter albumAdapter;

    public AlbumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_album, container, false);
        recyclerView=view.findViewById(R.id.recyleView);
        recyclerView.setHasFixedSize(true);
        albumAdapter = new AlbumAdapter(getContext(), FavoritesFragment.audio_list);
        recyclerView.setAdapter(albumAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        return view;
    }
}