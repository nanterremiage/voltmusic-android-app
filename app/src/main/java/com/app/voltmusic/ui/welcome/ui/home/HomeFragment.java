package com.app.voltmusic.ui.welcome.ui.home;

import android.app.ActionBar;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.*;
import android.widget.*;
import android.support.annotation.*;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.*;
import android.widget.Toast;
import com.android.volley.*;
import com.android.volley.toolbox.*;
import com.app.voltmusic.App;
import com.app.voltmusic.R;
import com.app.voltmusic.data.model.LoggedInUser;
import com.app.voltmusic.database.entite.Utilisateur;
import com.google.gson.Gson;
import org.json.*;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        return root;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onStart();
        RequestQueue queue = Volley.newRequestQueue(App.getContext());

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,
                "https://volt-music.herokuapp.com/utilisateurs",
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONArray users = ((JSONArray) (new JSONObject(response.get("_embedded").toString())).get("utilisateurs"));
                            for(int i = 0; i < users.length(); i++) {
                                System.out.println((new JSONObject(users.get(i).toString())).get("pseudo"));
                                View v = view;

                                // Find the LinearLayout
                                LinearLayout sv = (LinearLayout) v.findViewById(R.id.MusicianList);
                                ImageButton btn = new ImageButton(App.getContext());
                                btn.setBackground(Drawable.createFromPath("drawable://artiste1.jpeg"));
                                btn.setLayoutParams(new LinearLayout.LayoutParams(
                                        150, 150));
                                btn.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        System.out.println("Ouvrir le profil n°1");
                                    }
                                });

                                CardView cv = new CardView(App.getContext());
                                cv.setLayoutParams(new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        LinearLayout.LayoutParams.WRAP_CONTENT));
                                cv.setCardElevation(2.5F);
                                cv.setRadius(50F);

                                cv.addView(btn);
                                sv.addView(cv);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        if(false) {
                            System.out.println("Des données ont été trouvées.");
                        } else {
                            System.out.println("Aucune donnée en base.");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Erreur: " + error.toString());
                        Toast.makeText(App.getContext(), "Erreur lors de la récuperation des données.", Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();
                // headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", (new LoggedInUser()).getUserToken());
                return headers;
            }
        };

        queue.add(req);
    }
}