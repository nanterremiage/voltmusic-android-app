package com.app.voltmusic.ui.login;

import android.arch.lifecycle.*;
import android.util.Patterns;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.*;
import com.app.voltmusic.App;
import com.app.voltmusic.R;
import com.app.voltmusic.data.LoginRepository;
import com.app.voltmusic.database.entite.Utilisateur;
import com.app.voltmusic.database.repository.UtilisateurRepository;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class LoginViewModel extends ViewModel {

    private MutableLiveData<LoginFormState> loginFormState = new MutableLiveData<>();
    private MutableLiveData<LoginResult> loginResult = new MutableLiveData<>();
    private Utilisateur loggedUser = null;
    private LoginRepository loginRepository;
    private UtilisateurRepository utilisateurRepo = new UtilisateurRepository(App.getApplication());

    LoginViewModel(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    LiveData<LoginFormState> getLoginFormState() {
        return loginFormState;
    }
    LiveData<LoginResult> getLoginResult() {
        return loginResult;
    }

    public void login(String username, String password) {

        // can be launched in a separate asynchronous job
        RequestQueue queue = Volley.newRequestQueue(App.getContext());
        String url = "https://volt-music.herokuapp.com/login";
        Map<String, String> postParam = new HashMap<String, String>();
        postParam.put("email", username);
        postParam.put("password", password);

        JSONObject params = new JSONObject(postParam);

        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                url,
                params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        HashMap<String, String> resHeader;
                        HashMap<String, String> resBody;

                        try {
                            resHeader = new Gson().fromJson(response.get("header").toString(), HashMap.class);
                            resBody = new Gson().fromJson(response.get("body").toString(), HashMap.class);
                        } catch(JSONException e) {
                            resHeader = null;
                            resBody = null;
                            System.out.println("Erreur : " + e.getMessage());
                        }

                        if(!resHeader.isEmpty() && !resBody.isEmpty() && !resHeader.get("Authorization").isEmpty()) {
                            loggedUser = new Utilisateur();
                            loggedUser.userToken = resBody.get("pseudo");
                            loggedUser.pseudo = resBody.get("pseudo");
                            loggedUser.nom = resBody.get("nom");
                            loggedUser.prenom = resBody.get("prenom");
                            loggedUser.email = resBody.get("email");
                            loggedUser.numEtu = Integer.parseInt(resBody.get("num_etu"));
                            loggedUser.role = resBody.get("role");
                            loggedUser.dateNaissance = resBody.get("date_naissance");
                            loggedUser.userToken = resHeader.get("Authorization");
                            utilisateurRepo.removeUsers();
                            utilisateurRepo.addUser(loggedUser);

                            LoggedInUserView user = new LoggedInUserView(loggedUser.userToken,
                                loggedUser.pseudo, loggedUser.prenom, loggedUser.nom,
                                loggedUser.email, loggedUser.dateNaissance,
                                loggedUser.numEtu, loggedUser.role);

                            System.out.println("Réussite de la connexion.");
                            loginResult.setValue(new LoginResult(user));
                        } else {
                            System.out.println("Echec de la connexion.");
                            loginResult.setValue(new LoginResult(R.string.login_failed));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println("Erreur: " + error.toString());
                        loginResult.setValue(new LoginResult(R.string.login_failed));
                    }
                }) {
            @Override
            public Map<String, String> getHeaders()  {
                HashMap<String, String> headers = new HashMap<>();
                // headers.put("Accept", "application/json");
                headers.put("Content-Type", "application/json");
                return headers;
            }

            @Override
            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                try {
                    String jsonString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                    System.out.println(response.headers);
                    JSONObject result = new JSONObject();
                    result.put("header", new JSONObject(response.headers));
                    result.put("body", new JSONObject(jsonString));

                    return Response.success(result,
                            HttpHeaderParser.parseCacheHeaders(response));
                } catch (UnsupportedEncodingException e) {
                    return Response.error(new ParseError(e));
                } catch (JSONException je) {
                    return Response.error(new ParseError(je));
                }
            }
        };

        queue.add(req);
    }

    public void loginDataChanged(String username, String password) {
        if (!isUserNameValid(username)) {
            loginFormState.setValue(new LoginFormState(R.string.invalid_username, null));
        } else if (!isPasswordValid(password)) {
            loginFormState.setValue(new LoginFormState(null, R.string.invalid_password));
        } else {
            loginFormState.setValue(new LoginFormState(true));
        }
    }

    // A placeholder username validation check
    private boolean isUserNameValid(String username) {
        if (username == null) {
            return false;
        }
        if (username.contains("@")) {
            return Patterns.EMAIL_ADDRESS.matcher(username).matches();
        } else {
            return !username.trim().isEmpty();
        }
    }

    // A placeholder password validation check
    private boolean isPasswordValid(String password) {
        return password != null && password.trim().length() > 5;
    }
}