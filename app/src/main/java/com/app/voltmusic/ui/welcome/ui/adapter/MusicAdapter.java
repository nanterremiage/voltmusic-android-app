package com.app.voltmusic.ui.welcome.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.voltmusic.R;
import com.app.voltmusic.data.model.MusicFile;
import com.app.voltmusic.ui.welcome.ui.PlayerActivity.PlayerActivity;

import java.util.ArrayList;

//This adapter permits us to do the link between the playlist layout file and the music fragment which contains our songs

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.MusicViewHolder> {
    private Context context;
    private ArrayList<MusicFile> musicFiles;
    private int position ;
    public MusicAdapter(Context context, ArrayList<MusicFile> musicFiles) {
        this.context = context;
        this.musicFiles = musicFiles;
    }

    @NonNull
    @Override
    //Creation of the link with the layout file
    public MusicViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.playlist, viewGroup, false);
        return new MusicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicViewHolder musicViewHolder, final int i) {
        musicViewHolder.title.setText(musicFiles.get(i).getTitre());
        musicViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //We create a new intent that would direct us to the player activity
                Intent intent= new Intent(context, PlayerActivity.class);
                intent.putExtra("position",i);
                //Here we start the player activity when we click on one song
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return musicFiles.size();
    }

    public class MusicViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView imageView;

        public MusicViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.music_name);
            imageView = itemView.findViewById(R.id.playlist_img);
        }
    }
}
