package com.app.voltmusic.ui.welcome.ui.personal;

import android.Manifest;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.voltmusic.R;
import com.app.voltmusic.data.model.MusicFile;
import com.app.voltmusic.ui.welcome.WelcomeActivity;
import com.app.voltmusic.ui.welcome.ui.music_ablum_fragment.AlbumFragment;
import com.app.voltmusic.ui.welcome.ui.music_ablum_fragment.MusicFragment;
import com.app.voltmusic.ui.welcome.ui.adapter.MusicPagerAdapter;

import java.util.ArrayList;

public class FavoritesFragment extends Fragment {

    private FavoritesViewModel favoritesViewModel;
    public static int REQUEST_CODE = 1;
    public static ArrayList<MusicFile> audio_list;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        favoritesViewModel = ViewModelProviders.of(this).get(FavoritesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_favorites, container, false);
        ViewPager viewPager = (ViewPager) root.findViewById(R.id.view_pager);
        TabLayout tabLayout = (TabLayout) root.findViewById(R.id.tab_layout);
        MusicPagerAdapter musicPagerAdapter=new MusicPagerAdapter(getChildFragmentManager());
        musicPagerAdapter.addFragments(new MusicFragment(),"Songs");
        musicPagerAdapter.addFragments(new AlbumFragment(),"Album");
        viewPager.setAdapter(musicPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        return root;
    }


    @Override
    public void onStart() {
        super.onStart();
        permission();
    }



    private void permission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(((WelcomeActivity) getActivity()), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            audio_list = getAllMusic(getContext());

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                audio_list = getAllMusic(getContext());
            } else {
                ActivityCompat.requestPermissions(((WelcomeActivity) getActivity()), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
            }
        }
    }

    public static ArrayList<MusicFile> getAllMusic(Context context) {

        audio_list = new ArrayList<>();
        Uri url = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] music_data = {
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.DATE_ADDED,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATA,
        };
        Cursor cursor = context.getContentResolver().query(url, music_data, null, null, null);

        if (cursor != null) {
            while (cursor.moveToNext()) {
                String titre = cursor.getString(0);
                String album = cursor.getString(1);
                String artist = cursor.getString(2);
                String date_sortie = cursor.getString(3);
                String duration = cursor.getString(4);
                String path = cursor.getString(5);
                MusicFile music = new MusicFile(path, titre, artist, album, duration, date_sortie);
                audio_list.add(music);
            }
        }

        return audio_list;
    }
}