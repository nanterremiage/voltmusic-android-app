package com.app.voltmusic.ui.welcome.ui.user;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.voltmusic.App;
import com.app.voltmusic.MainActivity;
import com.app.voltmusic.R;
import com.app.voltmusic.database.repository.UtilisateurRepository;
import com.app.voltmusic.ui.login.LoginActivity;
import com.app.voltmusic.ui.welcome.WelcomeActivity;

public class LogoutFragment extends Fragment {

    private LogoutViewModel logoutViewModel;
    private UtilisateurRepository utilisateurRepo = new UtilisateurRepository(App.getApplication());

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        logoutViewModel =
                ViewModelProviders.of(this).get(LogoutViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        logoutViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                String goodbye = getString(R.string.goodbye);
                utilisateurRepo.removeUsers();
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                Toast.makeText(getContext(), goodbye, Toast.LENGTH_LONG).show();
            }
        });
        return root;
    }
}