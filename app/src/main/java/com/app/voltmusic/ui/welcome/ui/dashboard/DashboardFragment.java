package com.app.voltmusic.ui.welcome.ui.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.ViewModelProviders;
import com.app.voltmusic.R;
import com.app.voltmusic.data.model.LoggedInUser;
import com.app.voltmusic.data.model.MusicFile;
//import com.app.voltmusic.ui.welcome.ui.adapter.AlbumAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class DashboardFragment extends Fragment {

    private DashboardViewModel dashboardViewModel;
    private TextView textViewAlbumName;
    private Button newAlbumBtn;
    private LoggedInUser currentUser;


    RecyclerView recyclerViewEditAlbum;
    RecyclerView recyclerViewEditPlaylist;
    //AlbumAdapter albumAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        //albumAdapter = new AlbumAdapter(getContext(), getAllMusic());
        recyclerViewEditAlbum = root.findViewById(R.id.recyle_view_edit_album);
        recyclerViewEditAlbum.setHasFixedSize(true);
        //recyclerViewEditAlbum.setAdapter(albumAdapter);
        recyclerViewEditAlbum.setLayoutManager(new LinearLayoutManager(getContext(), recyclerViewEditAlbum.VERTICAL, false));

        recyclerViewEditPlaylist = root.findViewById(R.id.recyle_view_edit_playlist);
        recyclerViewEditPlaylist.setHasFixedSize(true);
        //recyclerViewEditPlaylist.setAdapter(albumAdapter);
        recyclerViewEditPlaylist.setLayoutManager(new LinearLayoutManager(getContext(), recyclerViewEditPlaylist.VERTICAL, false));

        currentUser = new LoggedInUser();
        TextView username = root.findViewById(R.id.username);
        TextView dateDeNaissance = root.findViewById(R.id.date_naissance);
        username.setText(currentUser.getNom()+" "+currentUser.getPrenom());
        String oldDateNaissance = currentUser.getDateNaissance();
        try {
            Date DateNaissanceParsed = new SimpleDateFormat("dd/MM/yyyy").parse(oldDateNaissance);
            dateDeNaissance.setText((CharSequence) DateNaissanceParsed);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        newAlbumBtn = (Button) root.findViewById(R.id.new_album_btn);
        newAlbumBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewAlbum.class);
                getContext().startActivity(intent);
            }
        });
        return root;
    }

    public static ArrayList<MusicFile> getAllMusic() {
        ArrayList<MusicFile> audio_list = new ArrayList<>();
        for (int i = 0; i < 10; i++)
        {
            String titre = "Titre " + i;
            String album = "Album " + i;
            String artist = "Artist " + i;
            String date_sortie = "Datesortie " + i;
            String duration = "Duration " + i;
            String path = "path " + i;
            MusicFile music = new MusicFile(path, titre, artist, album, duration, date_sortie);
            audio_list.add(music);
        }
        return audio_list;
    }
}

