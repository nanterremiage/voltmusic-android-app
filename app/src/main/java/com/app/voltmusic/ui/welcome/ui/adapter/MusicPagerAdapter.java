package com.app.voltmusic.ui.welcome.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class MusicPagerAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> music_fragments;
    public ArrayList<String> titles;
    public MusicPagerAdapter(FragmentManager fm) {
        super(fm);
        this.music_fragments=new ArrayList<>();
        this.titles=new ArrayList<>();
    }

    public void addFragments(Fragment fragments, String title){
        music_fragments.add(fragments);
        titles.add(title);
    }

    @Override
    public Fragment getItem(int i) {
        return music_fragments.get(i);
    }

    @Override
    public int getCount() {
        return music_fragments.size();
    }
    @Override
    public CharSequence getPageTitle(int position){
        return titles.get(position);
    }
}

