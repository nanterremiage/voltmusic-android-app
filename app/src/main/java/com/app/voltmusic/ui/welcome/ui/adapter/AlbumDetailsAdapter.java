package com.app.voltmusic.ui.welcome.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.voltmusic.R;
import com.app.voltmusic.data.model.MusicFile;
import com.app.voltmusic.ui.welcome.ui.AlbumDetails.AlbumDetails;

import java.util.ArrayList;

public class AlbumDetailsAdapter extends RecyclerView.Adapter<AlbumDetailsAdapter .MyHolder> {
    private Context context;
    private ArrayList<MusicFile> albumFiles;
    View view;

    public AlbumDetailsAdapter (Context context, ArrayList<MusicFile> albumFiles) {
        this.context = context;
        this.albumFiles = albumFiles;
    }

    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.playlist, viewGroup, false);
        return new MyHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyHolder myHolder, final int i) {
        myHolder.music_name.setText(albumFiles.get(i).getTitre());

    }

    @Override
    public int getItemCount() {
        return albumFiles.size();
    }

    public class MyHolder extends RecyclerView.ViewHolder {
        ImageView playlist_img;
        TextView music_name;
        public MyHolder(@NonNull View itemView) {
            super(itemView);
            playlist_img = itemView.findViewById(R.id.playlist_img);
            music_name = itemView.findViewById(R.id.music_name);
        }
    }
}
