package com.app.voltmusic.database.repository;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.os.AsyncTask;
import android.util.Log;

import com.app.voltmusic.App;
import com.app.voltmusic.database.VoltMusiqueDatabase;
import com.app.voltmusic.database.dao.UtilisateurDao;
import com.app.voltmusic.database.entite.Utilisateur;

public class UtilisateurRepository {

    private UtilisateurDao utilisateurDao;

    public UtilisateurRepository(Application application) {
        VoltMusiqueDatabase db = Room.databaseBuilder(App.getContext(),
                VoltMusiqueDatabase.class, "voltmusique").build();
        utilisateurDao = db.utilisateurDao();
    }

    public void addUser(Utilisateur user) {
        new insertAsyncTask(utilisateurDao).execute(user);
    }

    public Utilisateur getUser() {
        return new getUserAsyncTask(utilisateurDao).doInBackground();
    }

    public void removeUsers() {
        new deleteAsyncTask(utilisateurDao).execute();
    }

    private static class insertAsyncTask extends AsyncTask<Utilisateur, Void, Void> {
        private UtilisateurDao mAsyncTaskDao;

        insertAsyncTask(UtilisateurDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Utilisateur... params) {
            mAsyncTaskDao.insertUser(params[0]); // This line throws the exception
            Log.d(getClass().getSimpleName(), "do in background 1");
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Utilisateur, Void, Void> {
        private UtilisateurDao mAsyncTaskDao;

        deleteAsyncTask(UtilisateurDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Utilisateur... params) {
            mAsyncTaskDao.deleteAllUsers(); // This line throws the exception
            Log.d(getClass().getSimpleName(), "do in background 1");
            return null;
        }
    }

    private static class getUserAsyncTask extends AsyncTask<Utilisateur, Void, Utilisateur> {
        private UtilisateurDao mAsyncTaskDao;

        getUserAsyncTask(UtilisateurDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Utilisateur doInBackground(final Utilisateur... params) {
            return mAsyncTaskDao.getUser(); // This line throws the exception
        }
    }
}
