package com.app.voltmusic.database;

import android.arch.persistence.room.*;

import com.app.voltmusic.database.dao.UtilisateurDao;
import com.app.voltmusic.database.entite.Utilisateur;

@Database(entities = {Utilisateur.class}, version = 1, exportSchema = false)

@TypeConverters({Converters.class})
public abstract class VoltMusiqueDatabase extends RoomDatabase {
    public abstract UtilisateurDao utilisateurDao();
}
