package com.app.voltmusic.database.entite;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;
import android.support.annotation.*;

@Entity
public class Utilisateur {

    @PrimaryKey
    @NonNull
    public String userToken;

    @ColumnInfo(name = "pseudo")
    public String pseudo;

    @ColumnInfo(name = "prenom")
    public String prenom;

    @ColumnInfo(name = "nom")
    public String nom;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "date_naissance")
    public String dateNaissance;

    @ColumnInfo(name = "num_etu")
    public Integer numEtu;

    @ColumnInfo(name = "role")
    public String role;

}