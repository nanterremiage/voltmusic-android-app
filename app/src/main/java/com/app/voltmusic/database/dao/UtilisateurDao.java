package com.app.voltmusic.database.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;
import com.app.voltmusic.database.entite.*;
import java.util.*;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface UtilisateurDao {

    @Query("SELECT * FROM utilisateur LIMIT 1;")
    Utilisateur getUser();

    @Query("SELECT * FROM utilisateur WHERE usertoken = (:userId)")
    Utilisateur loadById(int userId);

    @Insert(onConflict = REPLACE)
    void insertUser(Utilisateur user);

    @Update
    void updateUser(Utilisateur user);

    @Query("DELETE FROM utilisateur;")
    void deleteAllUsers();

}
