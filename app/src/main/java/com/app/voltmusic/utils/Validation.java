package com.app.voltmusic.utils;

import java.util.regex.*;

public class Validation {
    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isValidPassword(String pass) {
        if (!pass.isEmpty() && pass.length() >= 6) {
            return true;
        }
        return false;
    }

    public static boolean isValidNumber(String num) {
        if (!num.isEmpty() && num.length() > 5) {
            return true;
        }
        return false;
    }

    public static boolean isValidName(String name) {
        if (!name.isEmpty() && name.length() > 5) {
            return true;
        }
        return false;
    }
}
